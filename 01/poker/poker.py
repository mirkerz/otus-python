#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# -----------------
# Реализуйте функцию best_hand, которая принимает на вход
# покерную "руку" (hand) из 7ми карт и возвращает лучшую
# (относительно значения, возвращаемого hand_rank)
# "руку" из 5ти карт. У каждой карты есть масть(suit) и
# ранг(rank)
# Масти: трефы(clubs, C), пики(spades, S), червы(hearts, H), бубны(diamonds, D)
# Ранги: 2, 3, 4, 5, 6, 7, 8, 9, 10 (ten, T), валет (jack, J), дама (queen, Q),
# король (king, K), туз (ace, A)
# Например: AS - туз пик (ace of spades), TH -
# дестяка черв (ten of hearts), 3C - тройка треф (three of clubs)

# Задание со *
# Реализуйте функцию best_wild_hand, которая принимает на вход
# покерную "руку" (hand) из 7ми карт и возвращает лучшую
# (относительно значения, возвращаемого hand_rank)
# "руку" из 5ти карт. Кроме прочего в данном варианте "рука"
# может включать джокера. Джокеры могут заменить карту любой
# масти и ранга того же цвета, в колоде два джокерва.
# Черный джокер '?B' может быть использован в качестве треф
# или пик любого ранга, красный джокер '?R' - в качестве черв и бубен
# любого ранга.

# Одна функция уже реализована, сигнатуры и описания других даны.
# Вам наверняка пригодится itertools
# Можно свободно определять свои функции и т.п.
# -----------------

import itertools


def hand_rank(hand):
    """Возвращает значение определяющее ранг 'руки'"""
    ranks = card_ranks(hand)
    if straight(ranks) and flush(hand):
        return (8, max(ranks))
    elif kind(4, ranks):
        return (7, kind(4, ranks), kind(1, ranks, exclude=kind(4, ranks)))
    elif kind(3, ranks) and kind(2, ranks, exclude=kind(3, ranks)):
        return (6, kind(3, ranks), kind(2, ranks, exclude=kind(3, ranks)))
    elif flush(hand):
        return (5, ranks)
    elif straight(ranks):
        return (4, max(ranks))
    elif kind(3, ranks):
        return (3, kind(3, ranks), ranks)
    elif two_pair(ranks):
        return (2, two_pair(ranks), ranks)
    elif kind(2, ranks):
        return (1, kind(2, ranks), ranks)
    else:
        return (0, ranks)


def card_ranks(hand):
    """Возвращает список рангов (его числовой эквивалент),
    отсортированный от большего к меньшему"""
    ranks_table = {str(i): i for i in range(1, 10)}
    ranks_table.update(dict((('T', 10), ('J', 11), ('Q', 12),
                             ('K', 13), ('A', 14))))
    ranks = [ranks_table[card[0]] for card in hand]
    return sorted(ranks)


def flush(hand):
    """Возвращает True, если все карты одной масти"""
    # Select first card suit
    suit = hand[0][1]
    return all((card[1] == suit) for card in hand[1:])


def straight(ranks):
    """Возвращает True, если отсортированные ранги формируют последовательность
    5ти, где у 5ти карт ранги идут по порядку (стрит)"""
    if len(ranks) < 5:
        return False
    counter = 1
    rank = ranks[0]
    for i in ranks[1:]:
        if i == rank + 1:
            counter += 1
            if counter == 5:
                return True
        else:
            counter = 1
        rank = i
    return False


def kind(n, ranks, exclude=None):
    """Возвращает первый ранг, который n раз встречается в данной руке.
    Возвращает None, если ничего не найдено"""
    # ranks is sorted we presume :thinking:
    if n == 1:
        return ranks[0]
    counter = 1
    rank = ranks[0]
    for i in ranks[1:]:
        if i == rank and i != exclude:
            counter += 1
            if counter == n:
                return rank
        else:
            counter = 1
        rank = i
    return None


def two_pair(ranks):
    """Если есть две пары, то возврщает два соответствующих ранга,
    иначе возвращает None"""
    pair_1 = kind(2, ranks)
    if pair_1 is None:
        return None
    leftovers = [rank for rank in ranks if rank > pair_1]
    if len(leftovers) < 2:
        return None
    pair_2 = kind(2, leftovers)
    if pair_2 is None:
        return None
    return [pair_1, pair_2]


def compare_secondary_rank(r1, r2):
    """
    Compare two ranks of cards and return True if r1 > r2.
    r1 and r2 should have same structure in terms of length,
    which element is integer or iterable all that stuff
    """

    for v1, v2 in itertools.izip(r1, r2):
        if isinstance(v1, int):
            if v1 == v2:
                continue
            else:
                return v1 > v2
        # If value isn't int, then we conpare value for set of cards sorted by
        # value. Iterate it backwards comparing elements and mark decision
        # for first difference
        for i in range(len(v1) - 1, -1, -1):
            # They are always the same length
            e1 = v1[i]
            e2 = v2[i]

            if e1 == e2:
                continue

            return e1 > e2
    return None


def best_hand(hand):
    """Из "руки" в 7 карт возвращает лучшую "руку" в 5 карт """
    variants = itertools.combinations(hand, 5)
    # Using -1 for main rank we never have situation when
    # variant_rank[0] == rank_main on first iteration
    rank_main = -1
    rank_secondary = 0
    hand_of_choice = None
    for variant in variants:
        variant_rank = hand_rank(variant)
        take_hand = False
        if variant_rank[0] == rank_main:
            take_hand = compare_secondary_rank(
                variant_rank[1:], rank_secondary)
        else:
            take_hand = variant_rank[0] > rank_main
        if take_hand:
            rank_main = variant_rank[0]
            rank_secondary = variant_rank[1:]
            hand_of_choice = variant
    return hand_of_choice


def replace_element(hand, values, index):
    if index is None:
        yield hand
    else:
        # result = hand.copy()
        result = [e for e in hand]
        for value in values:
            v = '%s%s' % value
            if v in result:
                continue
            result[index] = v
            yield result


def jocker_combinations(hand):
    # r = (*range(2, 10), 'T', 'J', 'Q', 'K', 'A')
    r = (2, 3, 4, 5, 6, 7, 8, 9, 'T', 'J', 'Q', 'K', 'A')
    # evaluate generator as it lead to some mad results othervise
    red = list(itertools.product(r, ('H', 'D')))
    black = list(itertools.product(r, ('C', 'S')))
    ri = None
    bi = None
    if '?R' in hand:
        ri = [i for i, v in enumerate(hand) if v == '?R'][0]
    if '?B' in hand:
        bi = [i for i, v in enumerate(hand) if v == '?B'][0]
    for red_hand in replace_element(hand, red, ri):
        for black_hand in replace_element(red_hand, black, bi):
            for variant in itertools.combinations(black_hand, 5):
                yield variant


def best_wild_hand(hand):
    """best_hand но с джокерами"""
    variants = jocker_combinations(hand)
    # Using -1 for main rank we never have situation when
    # variant_rank[0] == rank_main on first iteration
    rank_main = -1
    rank_secondary = 0
    hand_of_choice = None
    for variant in variants:
        variant_rank = hand_rank(variant)
        take_hand = False
        if variant_rank[0] == rank_main:
            take_hand = compare_secondary_rank(
                variant_rank[1:], rank_secondary)
        else:
            take_hand = variant_rank[0] > rank_main
        if take_hand:
            rank_main = variant_rank[0]
            rank_secondary = variant_rank[1:]
            hand_of_choice = variant
    return hand_of_choice


def test_best_hand():
    print('test_best_hand')
    assert (sorted(best_hand("6C 7C 8C 9C TC 5C JS".split()))
            == ['6C', '7C', '8C', '9C', 'TC'])
    assert (sorted(best_hand("TD TC TH 7C 7D 8C 8S".split()))
            == ['8C', '8S', 'TC', 'TD', 'TH'])
    assert (sorted(best_hand("JD TC TH 7C 7D 7S 7H".split()))
            == ['7C', '7D', '7H', '7S', 'JD'])
    print('OK')


def test_best_wild_hand():
    print("test_best_wild_hand...")
    assert (sorted(best_wild_hand("6C 7C 8C 9C TC 5C ?B".split()))
            == ['7C', '8C', '9C', 'JC', 'TC'])
    assert (sorted(best_wild_hand("TD TC 5H 5C 7C ?R ?B".split()))
            # == ['7C', 'TC', 'TC', 'TD', 'TH'])
            == ['7C', 'TC', 'TD', 'TH', 'TS'])
    assert (sorted(best_wild_hand("JD TC TH 7C 7D 7S 7H".split()))
            == ['7C', '7D', '7H', '7S', 'JD'])
    print('OK')


if __name__ == '__main__':
    test_best_hand()
    test_best_wild_hand()
