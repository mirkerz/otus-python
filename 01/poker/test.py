import unittest
import poker


class TestBasicMethods(unittest.TestCase):

    def test_flush(self):
        self.assertTrue(poker.flush('6C 7C'.split()))
        self.assertTrue(poker.flush('6C'.split()))
        self.assertFalse(poker.flush('6C 7H'.split()))

    def test_card_ranks(self):
        self.assertEqual(poker.card_ranks('6C 7C'.split()), [6, 7])
        self.assertEqual(poker.card_ranks('JC 7C'.split()), [7, 11])

    def test_straight(self):
        self.assertTrue(poker.straight(list(range(1, 10))))
        self.assertFalse(poker.straight([2, 3, 4, 6, 7, 8, 9, 11]))
        self.assertFalse(poker.straight([2, 3]))

    def test_kind(self):
        self.assertEqual(poker.kind(2, [1, 2, 2, 3, 4]), 2)
        self.assertEqual(poker.kind(3, [1, 2, 2, 3, 4]), None)

    def test_two_pair(self):
        self.assertEqual(poker.two_pair([2, 2, 4, 6, 7, 8, 8, 11]), [2, 8])
        self.assertEqual(poker.two_pair([2, 3, 4, 6, 7, 8, 8, 11]), None)

    def test_best_hand(self):
        self.assertEqual(
            sorted(poker.best_hand("6C 7C 8C 9C TC 5C JS".split())),
            ['6C', '7C', '8C', '9C', 'TC']
        )
        self.assertEqual(
            sorted(poker.best_hand("TD TC TH 7C 7D 8C 8S".split())),
            ['8C', '8S', 'TC', 'TD', 'TH']
        )
        self.assertEqual(
            sorted(poker.best_hand("JD TC TH 7C 7D 7S 7H".split())),
            ['7C', '7D', '7H', '7S', 'JD']
        )

    def test_best_wild_hand(self):
        self.assertEqual(
            sorted(poker.best_wild_hand("6C 7C 8C 9C TC 5C ?B".split())),
            ['7C', '8C', '9C', 'JC', 'TC']
        )
        self.assertEqual(
            sorted(poker.best_wild_hand("TD TC 5H 5C 7C ?R ?B".split())),
            ['7C', 'TC', 'TD', 'TH', 'TS']
            # ['7C', 'TC', 'TC', 'TD', 'TH']
        )
        self.assertEqual(
            sorted(poker.best_wild_hand("JD TC TH 7C 7D 7S 7H".split())),
            ['7C', '7D', '7H', '7S', 'JD']
        )


if __name__ == '__main__':
    unittest.main()
