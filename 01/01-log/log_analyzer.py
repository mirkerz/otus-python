#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Naive analyzer for nginx logs in serach of url and request_time
expected nginx log format:
log_format ui_short ''
    '$remote_addr  $remote_user $http_x_real_ip [$time_local]'
    '"$request" $status $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID"'
    '"$http_X_RB_USER" $request_time';
"""
import argparse
import re
import os
import json
import logging
import logging.config
import gzip

from collections import namedtuple
from functools import partial
from datetime import datetime


LOGGER = logging.getLogger(__name__)
CONFIG = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./log",
    "encoding": "utf-8",
    "error_threshold": 0.1,
    "log_to": "analyzer.log",
    "report_template": "report_template.html"
}

LogFile = namedtuple('LogFile', ['filename', 'date'])


class LogRecord(object):
    """
    Representation of log record as combination of url, array of request_time
    values and some methods to populate and extract basic stats.
    """

    def __init__(self, url):
        self.url = url
        self.counter = 0
        self.time_sum = 0
        self.rtimes = []

    def update(self, rtime):
        """
        Add rtime value to list. Nice place to update stats for given
        record: max, sum, etc. Altho some instances will be dropped and certain
        stats will require later evaluation anyway
        """
        self.counter += 1
        self.time_sum += rtime
        self.rtimes.append(rtime)

    def serialize(self, total_count, total_time):
        return {
            'url': self.url,
            'count': self.counter,
            'count_perc': "%.3f" % (100 * self.counter / total_count),
            'time_sum': "%.3f" % self.time_sum,
            'time_perc': "%.3f" % (100 * self.time_sum / total_time),
            'time_avg': "%.3f" % (self.time_sum / self.counter),
            'time_max': "%.3f" % max(self.rtimes),
            'time_med': "%.3f" % median(self.rtimes),
        }


def median(series, is_sorted=False):
    """
    Lazy implementation of median algorithm. Could be enhanced with fancy
    method or replaced with build in 3.4 statistics.median
    If parameter :is_sorted is True explicit sorting is omitted
    """
    if not is_sorted:
        data = sorted(series)
    else:
        data = series

    if len(data) % 2 == 1:
        return data[len(data) // 2]
    else:
        return ((data[len(data) // 2 - 1] + data[len(data) // 2]) * 0.5)


def load_config(config_filename):
    """ Load config from external file """
    try:
        with open(config_filename, 'r') as config_file:
            config = json.load(config_file)
    except OSError:
        RuntimeError("Can't read config file")
        raise
    except json.JSONDecodeError:
        RuntimeError(
            "Error parsing config file: make sure it's valid json")
        raise

    return config


def get_log_filename(log_dir):
    log_regexp_match = re.compile(r'^nginx\-access\-ui\.log\-(\d{8})(\.gz)?$')
    logfile = None
    for f in os.listdir(log_dir):
        match = re.match(log_regexp_match, f)
        if not match:
            continue
        date = match.group(1)
        try:
            file_date = datetime.strptime(date, '%Y%m%d')
        except ValueError:
            LOGGER.error('Failed to identify logfile timestamp: %s', f)
            continue
        if logfile is None or file_date > logfile.date:
            logfile = LogFile(os.path.join(log_dir, f), file_date)
    return logfile


def get_report_filename(log_file, report_dir):
    report_filename = 'report-%s.html' % log_file.date.strftime('%Y.%m.%d')
    report = os.path.join(report_dir, report_filename)
    return report


def read_nginx_log(log_filename, encoding, processor, error_threshold):
    openner = open
    if log_filename.endswith('.gz'):
        openner = partial(gzip.open, log_filename, mode='rt')
    else:
        openner = partial(open, log_filename, mode='r')
    error_count = 0
    with openner(encoding=encoding, errors='ignore') as log_file:
        for number, line in enumerate(log_file):
            try:
                yield extract_data_from_log_record(line)
            except Exception:
                LOGGER.error('Error reading logrecord line %s' % number)
                error_count += 1
                continue

    if number == 0:
        RuntimeError('Failed to read nginx log file')
    elif 1. * error_count / number > error_threshold:
        raise RuntimeError('To many errors reading nginx log')


def write_report(report_filename,
                 data,
                 template_filename):
    """
    Load content of template file into memory, replace placeholder $table_json
    with json representation of data
    """
    with open(template_filename, 'r') as template_file:
        template = template_file.read()
    template = template.replace('$table_json', json.dumps(data))

    # Create directories if needed.
    os.makedirs(os.path.dirname(report_filename), exist_ok=True)
    with open(report_filename, 'w') as report_file:
        report_file.write(template)


def extract_data_from_log_record(record):
    """
    Extract data from one line of log. Naive realisation imply that
    line is space separated and url is spaceless and stored in 8 column. While
    url is always last space separated column and represented as valid float.
    No explicit validation of url is made
    """
    bits = record.split(' ')
    data = {
        'rtime': float(bits[-1]),
        'url': bits[7],
    }
    return data


def analyse_log(log_filename, config):
    urls = {}
    stats = {
        'total_time': 0.,
        'total_count': 0,
    }
    number = 0
    records = read_nginx_log(
        log_filename,
        config['encoding'],
        processor=extract_data_from_log_record,
        error_threshold=config.get('error_threshold', float('+Inf')),
    )
    for number, data in enumerate(records):
        if data['url'] not in urls:
            urls[data['url']] = LogRecord(data['url'])
        urls[data['url']].update(data['rtime'])
        stats['total_time'] += data['rtime']
        stats['total_count'] += 1
    valid_records = sorted(
        urls.values(),
        key=lambda r: r.time_sum,
        reverse=True,
    )[:config['REPORT_SIZE']]
    return [record.serialize(**stats) for record in valid_records]


def main(*args):
    config = CONFIG.copy()

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--config", help="path to .json config file", default='./config.json')
    args = parser.parse_args()
    config.update(load_config(args.config))
    logging.basicConfig(
        filename=config.get('log_to', None),
        format='[%(asctime)s] %(levelname).1s %(message)s',
        datefmt='%Y.%m.%d %H:%M:%S',
    )
    log_file = get_log_filename(config['LOG_DIR'])
    if not log_file:
        return None
    report_filename = get_report_filename(log_file, config['REPORT_DIR'])
    if os.path.isfile(report_filename):
        # File exists -- we assume that we don't need to do anything
        return None

    if not report_filename:
        return None
    LOGGER.info('Load logs from %s', log_file.filename)
    data = analyse_log(log_file.filename, config)
    LOGGER.info('Write report to %s' % report_filename)
    write_report(report_filename, data, config['report_template'])


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        LOGGER.exception(repr(e))
