import os
import gzip
import tempfile
import unittest
import logging

import log_analyzer


logging.disable(logging.CRITICAL)


class TestBasicMethods(unittest.TestCase):

    def test_median(self):
        self.assertEqual(log_analyzer.median([1, 2, 3]), 2)
        self.assertEqual(log_analyzer.median([2, 4]), 3)

    def test_log_line_parser(self):
        record = (
            '1.199.4.96 -  - [29/Jun/2017:03:50:22 +0300] "GET'
            ' /api/v2/slot/4822/groups HTTP/1.1" 200 22 "-" "Lynx/2.8.8dev.9'
            ' libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-"'
            ' "1498697422-3800516057-4708-9752773" "2a828197ae235b0b3cb" 0.157'
        )
        data = log_analyzer.extract_data_from_log_record(record)
        self.assertTrue(data['rtime'] - 0.157 < 1e-3)
        self.assertEqual(data['url'], '/api/v2/slot/4822/groups')

    def test_broken_log_record(self):
        record = '                       qwe'
        self.assertRaises(
            ValueError,
            log_analyzer.extract_data_from_log_record,
            record
        )

        record = '  1.1'
        self.assertRaises(
            IndexError,
            log_analyzer.extract_data_from_log_record,
            record
        )

    def test_error_threshold(self):
        """ Test if we can read file with wrong formating """
        log = b'\n'.join(
            [b'1 2 3 4 5 6 7 8 9 1.1']*10 + [b'1.1']*10
        )

        with tempfile.NamedTemporaryFile() as f:
            f.write(log)
            f.flush()
            config = {
                'encoding': 'utf-8',
                'REPORT_SIZE': 3,
                'error_threshold': 0.4,
            }
            self.assertRaises(
                RuntimeError,
                log_analyzer.analyse_log,
                f.name,
                config
            )

            config = {
                'encoding': 'utf-8',
                'REPORT_SIZE': 3,
                'error_threshold': 0.6,
            }
            log_analyzer.analyse_log(f.name, config)

    def test_reading_file(self):
        """ Test if we can read file with error in codec """
        record = b' '.join((
            'a a a a a a a a a a a a '.encode('utf-8'),
            b'\xf1',
            '1.0'.encode('utf-8')
        ))
        log = b'\n'.join([record]*2)

        data = [{
            'url': 'a',
            'count': 2,
            'count_perc': '100.000',
            'time_sum': '2.000',
            'time_perc': '100.000',
            'time_avg': '1.000',
            'time_max': '1.000',
            'time_med': '1.000',
        }]

        with tempfile.NamedTemporaryFile() as f:
            f.write(log)
            f.flush()
            config = {'encoding': 'utf-8', 'REPORT_SIZE': 3}
            result = log_analyzer.analyse_log(f.name, config)
        self.assertEqual(result, data)

    def test_reading_zip_file(self):
        record = (
            '1.199.4.96 -  - [29/Jun/2017:03:50:22 +0300] "GET'
            ' URL HTTP/1.1" 200 22 "-" "Lynx/2.8.8dev.9'
            ' libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-"'
            ' "1498697422-3800516057-4708-9752773" "2a828197ae235b0b3cb" 0.42'
        )
        log = '\n'.join([record]*100)

        data = [{
            'url': 'URL',
            'count': 100,
            'count_perc': '100.000',
            'time_sum': '42.000',
            'time_perc': '100.000',
            'time_avg': '0.420',
            'time_max': '0.420',
            'time_med': '0.420',
        }]

        # It will not work on windows systems
        with tempfile.NamedTemporaryFile(suffix='.gz') as f:
            with gzip.open(f.name, 'wb') as gzfile:
                gzfile.write(log.encode('utf-8'))

            config = {'encoding': 'utf-8', 'REPORT_SIZE': 3}
            result = log_analyzer.analyse_log(f.name, config)
        self.assertEqual(result, data)

    def test_selection_log_file(self):
        file_names = [
            'nginx-access-ui.log-20190630.gz',
            'nginx-access-ui.log-20170630',
            'nginx-access-ui.log-20170630.bz2',
            'nginx-access-ui.log-20170630.tar.gz',
            'nginx-access-ui.log',
            'nginx-access-ui.log-30173630.gz',
        ]
        name_1 = 'nginx-access-ui.log-20190630.gz'
        name_2 = 'nginx-access-ui.log-20170630'

        with tempfile.TemporaryDirectory() as log_dir:
            for f in file_names:
                open(os.path.join(log_dir, f), 'w').close()

            logfile = log_analyzer.get_log_filename(log_dir)
            self.assertEqual(logfile.filename, os.path.join(log_dir, name_1))
            os.remove(os.path.join(log_dir, name_1))
            logfile = log_analyzer.get_log_filename(log_dir)
            self.assertEqual(logfile.filename, os.path.join(log_dir, name_2))


if __name__ == '__main__':
    unittest.main()
